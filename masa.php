<?php
session_start();
if(!isset($_SESSION['nombre_de_usuario'])){
    echo'
    <script> alert("Por favor, inicia sesión");
    window.location = "index.php";
    </script>';
    session_destroy();
    die();
}




?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Calculadora de IMC</title>
<link rel="stylesheet" href="estilo/style4.css">
<script>
    function calcularIMC() {
        var peso = parseFloat(document.getElementById("peso").value);
        var altura = parseFloat(document.getElementById("altura").value);

        if (isNaN(peso) || isNaN(altura) || altura <= 0) {
            document.getElementById("resultado").innerHTML = "Por favor, ingresa valores válidos.";
            return;
        }

        var imc = peso / (altura * altura);
        document.getElementById("resultado").innerHTML = "Tu IMC es: " + imc.toFixed(2);
    }
</script>
</head>
<body>
    <h1>Calculadora de IMC</h1>
    <label for="peso">Peso (kg):</label>
    <input type="number" id="peso" step="0.01"><br><br>
    <label for="altura">Altura (m):</label>
    <input type="number" id="altura" step="0.01"><br><br>
    <button onclick="calcularIMC()">Calcular IMC</button>
    <p id="resultado"></p>
    <h2>Niveles de IMC</h2>
    <ul>
        <li>Bajo peso: IMC menor a 18.5</li>
        <li>Normal (Peso saludable): IMC de 18.5 a 24.9
        </li>
        <li>Sobrepeso: IMC de 25 a 29.9</li>
        <li>Obesidad (Grado I): IMC de 30 a 34.9</li>
        <li>Obesidad (Grado II): IMC de 35 a 39.9</li>
        <li>Obesidad (Grado III): IMC de 40 o más (obesidad severa)</li>
        <p>Tené en cuenta que el IMC no distingue entre la grasa y la masa magra. Dos personas con el mismo IMC podrían tener composiciones corporales muy diferentes.</p>
    
    </ul>
</body>
</html>
