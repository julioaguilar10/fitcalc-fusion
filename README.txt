ESTE PROYECTO ESTA BAJO LICENCIA GPL.
FitCalc Fusion
¿Qué es FitCalc Fusion?
Es un proyecto de software libre que brinda un sistema de inicio de sesión, registro y unas cuantas herramientas para realizar cálculos relacionados con la activad física. 
El objetivo de este proyecto es que con el tiempo se vayan sumando más herramientas que ayuden a realizar y hacer más certeros los cálculos ya nombrados o también que se añadan otros apartados relacionados con el entrenamiento o el asesoramiento para hacer el mismo.
Para el desarrollo de este proyecto se utilizó: html5, JavaScript, php, css, mysql y XAMPP.
Los paso para ejecutar el código en Linux se compartirán próximamente.  
Pasos para usar el código en Windows 
1.	Descargar e instalar xampp https://www.apachefriends.org/es/index.html
2.	Una vez instalado el programa del paso anterior, colocar los archivos en la carpeta htdocs. Si se instaló el programa en la misma partición que se encuentra el Sistema Operativo, la carpeta se encuentra en C:\xampp\htdocs
3.	Ejecutar el programa xampp e iniciar apache y mysql
4.	Ingresar a http://localhost/phpmyadmin/ desde el navegador. 
5.	Crear una base de datos llamada una “usuarios” y una tabla con el nombre de “registros”. La misma debe tener 3 columnas: ID(debe ser auto incremental), nombre de usuario y contrasena. 
6.	Luego de seguir los pasos anteriores se podrá probar la página ingresando a http://localhost/
