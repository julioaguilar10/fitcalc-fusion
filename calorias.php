<?php
session_start();
if(!isset($_SESSION['nombre_de_usuario'])){
    echo'
    <script> alert("Por favor, inicia sesión");
    window.location = "index.php";
    </script>';
    session_destroy();
    die();
}




?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Calculadora de Consumo Calórico Diario</title>
<link rel="stylesheet" href="estilo/style3.css">
<script>
    function calcularConsumoCalorico() {
        var edad = parseInt(document.getElementById("edad").value);
        var genero = document.getElementById("genero").value;
        var peso = parseFloat(document.getElementById("peso").value);
        var altura = parseFloat(document.getElementById("altura").value);
        var actividad = parseFloat(document.getElementById("actividad").value);

        if (isNaN(edad) || isNaN(peso) || isNaN(altura) || isNaN(actividad) || edad <= 0 || peso <= 0 || altura <= 0 || actividad <= 0) {
            document.getElementById("resultado").innerHTML = "Por favor, ingresa valores válidos.";
            return;
        }

        var tasaMetabolicaBasal = genero === "masculino" ? 88.362 + (13.397 * peso) + (4.799 * altura * 100) - (5.677 * edad) : 447.593 + (9.247 * peso) + (3.098 * altura * 100) - (4.330 * edad);
        var consumoCaloricoDiario = tasaMetabolicaBasal * actividad;
        
        document.getElementById("resultado").innerHTML = "Tu consumo calórico diario aproximado es: " + consumoCaloricoDiario.toFixed(2) + " calorías";
    }
</script>
</head>
<body>
    <h1>Calculadora de Consumo Calórico Diario</h1>
    <label for="edad">Edad:</label>
    <input type="number" id="edad"><br><br>
    <label for="genero">Género:</label>
    <select id="genero">
        <option value="masculino">Masculino</option>
        <option value="femenino">Femenino</option>
    </select><br><br>
    <label for="peso">Peso (kg):</label>
    <input type="number" id="peso"><br><br>
    <label for="altura">Altura (m):</label>
    <input type="number" id="altura"><br><br>
    <label for="actividad">Nivel de Actividad:</label>
    <select id="actividad">
        <option value="1.2">Sedentario (poco o ningún ejercicio)</option>
        <option value="1.375">Ligera actividad (ejercicio ligero/deportes 1-3 días/semana)</option>
        <option value="1.55">Moderada actividad (ejercicio moderado/deportes 3-5 días/semana)</option>
        <option value="1.725">Activo (ejercicio intenso/deportes 6-7 días/semana)</option>
        <option value="1.9">Muy activo (ejercicio muy intenso, trabajo físico o entrenamiento doble)</option>
    </select><br><br>
    <button onclick="calcularConsumoCalorico()">Calcular Consumo Calórico Diario</button>
    <p id="resultado"></p>
</body>
</html>
