<?php
session_start();
if(!isset($_SESSION['nombre_de_usuario'])){
    echo'
    <script> alert("Por favor, inicia sesión");
    window.location = "index.php";
    </script>';
    session_destroy();
    die();
}




?>



<!DOCTYPE html>
<html>
<head>
    <title>Calculadora de Velocidad Promedio</title>
    <link rel="stylesheet" href="estilo/style2.css">
</head>
<body>
    <h1>Calculadora de Velocidad Promedio</h1>
    
    <label for="distancia">Distancia recorrida (en metros):</label>
    <input type="number" id="distancia" step="0.01">
    <br>
    
    <label for="tiempo">Tiempo transcurrido (en minutos):</label>
    <input type="number" id="tiempo" step="0.01">
    <br>
    
    <button onclick="calcularVelocidad()">Calcular Velocidad Promedio</button>
    
    <p id="resultado"></p>
    
    <script>
        function calcularVelocidad() {
            var distanciaMetros = parseFloat(document.getElementById("distancia").value);
            var tiempoMinutos = parseFloat(document.getElementById("tiempo").value);
            
            if (isNaN(distanciaMetros) || isNaN(tiempoMinutos)) {
                document.getElementById("resultado").textContent = "Ingresa valores válidos para distancia y tiempo.";
            } else {
                var distanciaKilometros = distanciaMetros / 1000;
                var tiempoHoras = tiempoMinutos / 60;
                var velocidadPromedio = distanciaKilometros / tiempoHoras;
                document.getElementById("resultado").textContent = "Tu velocidad promedio es: " + velocidadPromedio.toFixed(2) + " km/h";
            }
        }
    </script>
    <a href="masa.php" class="calc-buttons">Calcúla tu IMC</a>
    <a href="calorias.php" class="calc-buttons1">Calcúla tu consumo calórico</a>
    <p>Este es un proyecto de software libre. Si deseas usarlo haz clic <a href="https://github.com/JulioAguilar10/login-register-average-speed-calculator.git">Aquí</a></p>
    <a href="cerrar_sesion.php">Cerrar sesión</a>
</body>
</html>
